﻿namespace lr1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lstbShapes = new System.Windows.Forms.ListBox();
            this.btnAddSquare = new System.Windows.Forms.Button();
            this.btnAddCircle = new System.Windows.Forms.Button();
            this.btnAddEllipse = new System.Windows.Forms.Button();
            this.btnAddRectangle = new System.Windows.Forms.Button();
            this.btnAddTriangle = new System.Windows.Forms.Button();
            this.btnAddLine = new System.Windows.Forms.Button();
            this.btnDraw = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.btnClear = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // lstbShapes
            // 
            this.lstbShapes.FormattingEnabled = true;
            this.lstbShapes.Location = new System.Drawing.Point(761, 0);
            this.lstbShapes.Name = "lstbShapes";
            this.lstbShapes.Size = new System.Drawing.Size(125, 329);
            this.lstbShapes.TabIndex = 0;
            this.lstbShapes.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.lstbShapes_KeyPress);
            // 
            // btnAddSquare
            // 
            this.btnAddSquare.Location = new System.Drawing.Point(680, 13);
            this.btnAddSquare.Name = "btnAddSquare";
            this.btnAddSquare.Size = new System.Drawing.Size(75, 23);
            this.btnAddSquare.TabIndex = 1;
            this.btnAddSquare.Text = "Add Square";
            this.btnAddSquare.UseVisualStyleBackColor = true;
            this.btnAddSquare.Click += new System.EventHandler(this.btnAddSquare_Click);
            // 
            // btnAddCircle
            // 
            this.btnAddCircle.Location = new System.Drawing.Point(680, 42);
            this.btnAddCircle.Name = "btnAddCircle";
            this.btnAddCircle.Size = new System.Drawing.Size(75, 23);
            this.btnAddCircle.TabIndex = 2;
            this.btnAddCircle.Text = "Add Circle";
            this.btnAddCircle.UseVisualStyleBackColor = true;
            this.btnAddCircle.Click += new System.EventHandler(this.btnAddCircle_Click);
            // 
            // btnAddEllipse
            // 
            this.btnAddEllipse.Location = new System.Drawing.Point(680, 71);
            this.btnAddEllipse.Name = "btnAddEllipse";
            this.btnAddEllipse.Size = new System.Drawing.Size(75, 23);
            this.btnAddEllipse.TabIndex = 3;
            this.btnAddEllipse.Text = "Add Ellipse";
            this.btnAddEllipse.UseVisualStyleBackColor = true;
            this.btnAddEllipse.Click += new System.EventHandler(this.btnAddEllipse_Click);
            // 
            // btnAddRectangle
            // 
            this.btnAddRectangle.Location = new System.Drawing.Point(680, 100);
            this.btnAddRectangle.Name = "btnAddRectangle";
            this.btnAddRectangle.Size = new System.Drawing.Size(75, 23);
            this.btnAddRectangle.TabIndex = 4;
            this.btnAddRectangle.Text = "Add Rect";
            this.btnAddRectangle.UseVisualStyleBackColor = true;
            this.btnAddRectangle.Click += new System.EventHandler(this.btnAddRectangle_Click);
            // 
            // btnAddTriangle
            // 
            this.btnAddTriangle.Location = new System.Drawing.Point(680, 129);
            this.btnAddTriangle.Name = "btnAddTriangle";
            this.btnAddTriangle.Size = new System.Drawing.Size(75, 23);
            this.btnAddTriangle.TabIndex = 5;
            this.btnAddTriangle.Text = "Add Triangle";
            this.btnAddTriangle.UseVisualStyleBackColor = true;
            this.btnAddTriangle.Click += new System.EventHandler(this.btnAddTriangle_Click);
            // 
            // btnAddLine
            // 
            this.btnAddLine.Location = new System.Drawing.Point(680, 158);
            this.btnAddLine.Name = "btnAddLine";
            this.btnAddLine.Size = new System.Drawing.Size(75, 23);
            this.btnAddLine.TabIndex = 6;
            this.btnAddLine.Text = "Add Line";
            this.btnAddLine.UseVisualStyleBackColor = true;
            this.btnAddLine.Click += new System.EventHandler(this.btnAddLine_Click);
            // 
            // btnDraw
            // 
            this.btnDraw.Location = new System.Drawing.Point(680, 188);
            this.btnDraw.Name = "btnDraw";
            this.btnDraw.Size = new System.Drawing.Size(75, 23);
            this.btnDraw.TabIndex = 7;
            this.btnDraw.Text = "DRAW";
            this.btnDraw.UseVisualStyleBackColor = true;
            this.btnDraw.Click += new System.EventHandler(this.btnDraw_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(12, 15);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(654, 315);
            this.pictureBox1.TabIndex = 8;
            this.pictureBox1.TabStop = false;
            // 
            // btnClear
            // 
            this.btnClear.Location = new System.Drawing.Point(680, 218);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(75, 23);
            this.btnClear.TabIndex = 9;
            this.btnClear.Text = "CLEAR";
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(887, 349);
            this.Controls.Add(this.btnClear);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.btnDraw);
            this.Controls.Add(this.btnAddLine);
            this.Controls.Add(this.btnAddTriangle);
            this.Controls.Add(this.btnAddRectangle);
            this.Controls.Add(this.btnAddEllipse);
            this.Controls.Add(this.btnAddCircle);
            this.Controls.Add(this.btnAddSquare);
            this.Controls.Add(this.lstbShapes);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListBox lstbShapes;
        private System.Windows.Forms.Button btnAddSquare;
        private System.Windows.Forms.Button btnAddCircle;
        private System.Windows.Forms.Button btnAddEllipse;
        private System.Windows.Forms.Button btnAddRectangle;
        private System.Windows.Forms.Button btnAddTriangle;
        private System.Windows.Forms.Button btnAddLine;
        private System.Windows.Forms.Button btnDraw;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button btnClear;
    }
}

