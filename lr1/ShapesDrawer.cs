﻿using lr1.Shapes;
using System.Collections.Generic;

namespace lr1
{
    class ShapesDrawer
    {
        private static ShapesDrawer instance;
        private ShapesDrawer()
        {

        }
        public static ShapesDrawer Instance
        {
            get
            {
                if(instance == null)
                {
                    instance = new ShapesDrawer();
                }
                return instance;
            }
        }
        public void DrawShapes(List<AbstractShape> shapes)
        {
            foreach(var shape in shapes)
            {
                shape.Draw();
            }
        }
    }
}
