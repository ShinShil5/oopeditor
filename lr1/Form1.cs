﻿using lr1.Shapes;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace lr1
{
    public partial class Form1 : Form
    {
        List<AbstractShape> CollectShapes(Graphics e)
        {
            List<AbstractShape> result = new List<AbstractShape>();
            foreach(var item in this.lstbShapes.Items) {
                ShapesFactory.ShapesTypes shapeType;
                Enum.TryParse(item.ToString(), true, out shapeType);
                AbstractShape shape = ShapesFactory.GetShape(shapeType);
                shape.G = e;
                shape.Pen = new Pen(Color.Black, 3);
                result.Add(shape);
            }
            return result;
        }
        public Form1()
        {
            InitializeComponent();

            pictureBox1.Image = new Bitmap(pictureBox1.Width, pictureBox1.Height);
        }
        
        private void btnDraw_Click(object sender, EventArgs e)
        {
            var g = Graphics.FromImage(pictureBox1.Image);
            ShapesDrawer.Instance.DrawShapes(this.CollectShapes(g));
            pictureBox1.Refresh();
        }

        private void btnAddSquare_Click(object sender, EventArgs e)
        {
            lstbShapes.Items.Add(ShapesFactory.ShapesTypes.SQUARE.ToString());
        }

        private void btnAddCircle_Click(object sender, EventArgs e)
        {
            lstbShapes.Items.Add(ShapesFactory.ShapesTypes.CIRCLE.ToString());
        }

        private void btnAddLine_Click(object sender, EventArgs e)
        {
            lstbShapes.Items.Add(ShapesFactory.ShapesTypes.LINE.ToString());
        }

        private void btnAddTriangle_Click(object sender, EventArgs e)
        {
            lstbShapes.Items.Add(ShapesFactory.ShapesTypes.TRIANGLE.ToString());
        }

        private void btnAddRectangle_Click(object sender, EventArgs e)
        {
            lstbShapes.Items.Add(ShapesFactory.ShapesTypes.RECTANGLE.ToString());
        }

        private void btnAddEllipse_Click(object sender, EventArgs e)
        {
            lstbShapes.Items.Add(ShapesFactory.ShapesTypes.ELLIPS.ToString());
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            pictureBox1.Image = null;
            pictureBox1.Image = new Bitmap(pictureBox1.Width, pictureBox1.Height);
        }

        private void lstbShapes_KeyPress(object sender, KeyPressEventArgs e)
        {
            if(e.KeyChar =='c' || e.KeyChar == 'с')
            {
                lstbShapes.Items.Clear();
            }else if (e.KeyChar == 'd' || e.KeyChar == 'в') 
            {
                if(lstbShapes.SelectedItem != null)
                {
                    lstbShapes.Items.Remove(lstbShapes.SelectedItem);
                }
            }
            lstbShapes.Invalidate();
        }
    }
}
