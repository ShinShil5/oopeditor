﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace lr1.Shapes
{
    abstract class AbstractShape
    {
        protected Graphics g;
        protected Pen pen;
        static protected Random rnd = new Random();
        public Graphics G
        {
            set
            {
                g = value;
            }
        }
        public Pen Pen
        {
            set
            {
                pen = value;
            }
        }
        public AbstractShape()
        {

        }
        public AbstractShape(Graphics e, Pen pen)
        {
            this.g = e;
            this.pen = pen;
        }
        public static Point getRandomPoint()
        {
            int x = rnd.Next(40, 400);
            int y = rnd.Next(40, 200);
            return new Point(x, y);
        }
        public static Point getBottomRightCorner(Point point)
        {
            int width = rnd.Next(10, 40);
            int height = rnd.Next(10, 40);
            return new Point(point.X + width, point.Y + height);
        }
        

        public abstract void Draw();
    }
}
