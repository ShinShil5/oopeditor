﻿using System.Drawing;
using System.Windows.Forms;

namespace lr1.Shapes
{
    class Circle : AbstractShape
    {
        public Circle() { }
        public Circle(Graphics e, Pen pen) : base(e, pen)
        {
        }

        public override void Draw()
        {
            Point tl = getRandomPoint();
            Point tb = getBottomRightCorner(tl);
            g.DrawEllipse(pen, tl.X, tl.Y, tb.X - tl.X, tb.X - tl.X);
        }
    }
}
