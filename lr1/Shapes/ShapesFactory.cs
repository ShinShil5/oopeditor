﻿namespace lr1.Shapes
{
    class ShapesFactory
    {
        public enum ShapesTypes
        {
            SQUARE,
            RECTANGLE,
            CIRCLE,
            ELLIPS,
            LINE,
            TRIANGLE
        }
        public static AbstractShape GetShape(ShapesTypes shapeType)
        {
            switch(shapeType)
            {
                case ShapesTypes.SQUARE:
                    return new Square();
                case ShapesTypes.RECTANGLE:
                    return new Rectangle();
                case ShapesTypes.TRIANGLE:
                    return new Triangle();
                case ShapesTypes.ELLIPS:
                    return new Ellipse();
                case ShapesTypes.CIRCLE:
                    return new Circle();
                case ShapesTypes.LINE:
                    return new Line();
                default:
                    return null;
            }
        }
        
    }
}
