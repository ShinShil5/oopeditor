﻿using System;
using System.Windows.Forms;
using System.Drawing;

namespace lr1.Shapes
{
    class Square : AbstractShape
    {
        public Square() : base() { }
        public Square(Graphics e, Pen pen) : base(e, pen)
        {
        }

        public override void Draw()
        {
            Point tl = getRandomPoint();
            Point tb = getBottomRightCorner(tl);
            g.DrawRectangle(pen, tl.X, tl.Y, tb.X - tl.X, tb.X - tl.X);
        }

    }
}
