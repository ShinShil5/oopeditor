﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace lr1.Shapes
{
    class Triangle : AbstractShape
    {
        public Triangle() { }
        public Triangle(Graphics e, Pen pen) : base(e, pen)
        {
        }
        private Point GetRandomMiddlePoint(Point point)
        {
            int width = rnd.Next(10, 40);
            int height = rnd.Next(10, 40);
            return new Point(point.X - width, point.Y + height);
        }
        public override void Draw()
        {
            Point tl = getRandomPoint();
            Point tb = getBottomRightCorner(tl);
            Point t3 = GetRandomMiddlePoint(tl);
            g.DrawPolygon(pen, new Point[] { tl, tb, t3 } );
        }
    }
}
