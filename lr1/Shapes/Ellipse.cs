﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace lr1.Shapes
{
    class Ellipse : AbstractShape
    {
        public Ellipse() { }
        public Ellipse(Graphics e, Pen pen) : base(e, pen)
        {
        }

        public override void Draw()
        {
            Point tl = getRandomPoint();
            Point tb = getBottomRightCorner(tl);
            g.DrawRectangle(pen, tl.X, tl.Y, tb.X - tl.X, tb.Y - tl.Y);
        }
    }
}
