﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace lr1.Shapes
{
    class Line : AbstractShape
    {
        public Line() { }
        public Line(Graphics e, Pen pen) : base(e, pen)
        {
        }

        public override void Draw()
        {
            Point tl = getRandomPoint();
            Point tb = getBottomRightCorner(tl);
            g.DrawLine(pen, tl, tb);
        }
    }
}
